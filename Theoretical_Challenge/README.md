Scott Crooks - Dalia Tech Challenge
===================

----------

# Theoretical Challenge

Hello Dalia Tech team! When I was reading the first line of the theoretical challenge, which is...

> We need our web application to auto scale according to the amount of traffic ‐ please describe how you would do this.

...I immediately thought: what's the budget we have to spend on resources? Engineers in any organization are naturally limited by what the C-level / business end of the company determines what the monthly IT budget should be. Therefore, for this first challenge, I figured I would present 3 alternatives for what I would recommend if confronted with this situation in real-life. This also provides me an opportunity to show you all what I know about infrastructure at this point.

Those three alternatives are:

 1. Cloud Premium
 2. Cloud Budget **OR** Cloud with more control from DevOps
 3. Self-Hosted in Datacenter

I will explain in more detail what each of those alternatives mean in their respective sections. At the end of providing an explanation of all of these services, I will answer the 7 questions.

Assumptions
-------------
I wouldn't be an engineer if I didn't clearly state my assumptions. I will make note of them here as I go along in the process.

* Dalia is not going the route of having a managed infrastructure provider like Rackspace. We run everything ourselves, whether in the cloud, or in a physical datacenter.
* Dalia has some sort of monitoring solution in place for the sake of these examples, and that it is Docker-friendly.
* Deployment strategy is flexible enough to be changed. I am also assuming the codebase allows for changes i.e. is microservice friendly.

Questions to consider for all solutions
-------------

### Docker Swarm or Kubernetes?

My previous company, Vacasa, used Docker Swarm since they were a bit late to the Docker world, and therefore, had the freedom to choose between Docker Swarm and Kubernetes. Previously that **wasn't** the case: it was only Kubernetes for a time. Kubernetes was first to the game, and therefore, is more developed and is better at auto-scaling *for the containers themselves*. I have knowledge of this because while during research at Vacasa, myself and the other DevOps engineer at the time decided to go the Docker Swarm route. We made the choice because Docker Swarm was more native, and we liked the road map that Docker Swarm had for the future (example: built-in secrets management).

Docker Swarm's inability for *automatic* horizontal scaling at the moment is discussed here:

* [How to configure autoscaling on docker swarm?](https://stackoverflow.com/questions/41668621/how-to-configure-autoscaling-on-docker-swarm)
* [Auto scaling like kubernetes horizontal](https://github.com/docker/swarm/issues/2151)

With that said, *I will discuss in the solutions the idea of using Docker Swarm product since I have experience*. I will also present alternatives / ideas to how to auto-scale Docker Swarm in the absence of an official solution from Docker themselves. However, Kubernetes is something that could also be done, and I think for the purposes of Dalia, actually might be a better idea.

### Deployment strategy?

Based on my experience, you can deploy your application in many ways, but usually they fall into the following categories:

* Spin up brand new infrastructure, deploy to new infra, do blue/green testing (via DNS weighting, or percentage weighting with load balancers), wait for confirmation that new version works, destroy old infrastructure. Repeat.
* Run a Kubernetes / Docker Swarm cluster (which is semi-static), and deploy new containers using rolling updates. In Docker Swarm, it goes something like this:

```
docker service create \
    --replicas 3 \
    --name redis \
    --update-delay 10s \
    redis:3.0.6
```

Personally I would advocate for the second option. Elastic Beanstalk deployments are usually more like the first option, and this is fine. However, if we're talking about having a very good CI/CD pipeline setup, this means we're deploying quickly and perhaps more than 10 times per day. A Swarm or Kubernetes cluster is better suited for this option.

Option 1: Cloud Premium
-------------

AWS comes with a myriad of services that are possible to use, which means a DevOps team doesn't necessarily need to do a ton of "ops" work, and they can instead focus on testing, deployments, and the application itself.

The company I came from, Vacasa, was very much of the mentality of paying for SaaS products instead of choosing an open-source product, and running it ourselves. We still ran much of our stuff (see Option 2!) ourselves though. If money was no object, then the following would be useful:

* Docker Datacenter (DC) or EC2 Container Service (ECS)
* New Relic / DataDog for a monitoring solution
* RabbitMQ as a service (like [CloudAMPQ](https://www.cloudamqp.com/) or [IronMQ](https://www.iron.io/platform/ironmq/)) for queueing needs
* [Sentry.io](https://sentry.io/welcome/) for error tracking
* [CircleCI](https://circleci.com/) or [CodeShip](https://codeship.com/) for CI and testing
* [Buddy.works](https://buddy.works/) or [DeployBot](https://deploybot.com/) for deployments

The list could continue, but I believe that these are essential for the needs of most companies. Obviously you all are hiring me because money **isn't** unlimited, but I think these are useful to mention because the team I was in at Vacasa evaluated many of these products (and even used some of them) during our infrastructure planning.

I would like to make another point about these kind of services: in my opinion, there should be an internal discussion about the pros and cons of using SaaS products on a case-by-case basis. They are convenient and fast, with the trade-off of outsourcing that knowledge elsewhere, or getting locked-in (AWS services in particular). I think the healthiest option is somewhere in the middle: use SaaS products where it makes sense, and run infrastrucutre in AWS yourself also when it makes sense.

Option 2: Cloud Budget
-------------

This option is called "budget" because it assumes that room in the budget is sufficient enough to for using *some* services from the cloud provider, but is also reasonable in cost and relies on the engineers themselves to handle more of the infrastructure. Since you all are running AWS, for me the budget version would involve more usage of AWS Auto-scaling groups and/or Elastic Beanstalk. Using Auto-scaling groups assumes that the engineers in the DevOps team want more visibility into the underlying infrastructure. Elastic Beanstalk is very developer focused, and is useful when you don't want to see any parts of the underlying infrastructure. 

Elastic Beanstalk is admittedly not very interesting here. There are a [myriad](http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/using-features.CNAMESwap.html) of [articles](http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/using-features.managing.as.html) showing [how](http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/using-features.managing.as.html) one can auto-scale with Elastic Beanstalk automatically using the AWS interface. Frankly, this also doesn't allow me to demonstrate any ability really.

So, if we're using Auto-scaling groups in AWS, my setup would look something like this:

[![Cloud Budget](https://gitlab.com/sc250024/dalia-tech-challenge/uploads/1f6aac0f7a7e8ef9a76b344efc662c70/dalia_autoscaling_cloud_budget_small.png)](https://gitlab.com/sc250024/dalia-tech-challenge/uploads/d17ce396f4951495097588f6975594c7/dalia_autoscaling_cloud_premium.png)

Some details I would also like to mention:

* Each service has it's own ELB/ALB with SSL termination if needed.
* Each service has it's own `docker-compose.yml` file and corresponding code repository, which is used for deployment. That file also contains the listening port on the Docker Swarm side, which is unique to each service. Something in the range between 20000 - 30000 would work. The ELB/ALB would forward all traffic to this port on a Swarm manager, which would handle the routing to the appropriate container(s).
* Cluster has access to appropriate RDS database instances when needed.

### Auto-scaling the number of containers

Docker provides a way to scale the number of containers running in the swarm using the `docker service scale` command: [Scale the service in the swarm](https://docs.docker.com/engine/swarm/swarm-tutorial/scale-service/). However, this is a manual step which requires an engineer. In order to this automatically, in the end you need a monitoring solution and some manual scripting if you're using Docker Swarm; Kubernetes does this automatically. However, I also believe scaling the **number of containers** themselves is not as important as scaling the underlying EC2 instances. The reason? Assuming I have continuous deployment, or even daily deployments, and I'm running Docker Swarm, I could simply change the number of replicas in the `docker-compose.yml` file, and it would take effect on the next deployment automatically. When the number of containers running on the cluster becomes sufficiently great, then it's time to auto-scale the EC2 instances.

### Auto-scaling the cluster EC2 instances

Adding more worker nodes to the Docker Swarm auto-scaling group is a more interesting and achievable problem.

**AWS Auto-scaling**

AWS comes with their auto-scaling solution, but it must be configured first with a Launch Configuration, an associated AMI, and a scaling policy. If I were to go this route, I would build the AMI images with [HashiCorp Packer](https://www.packer.io/intro/index.html) that automatically provisions a new node to join the Swarm cluster. It would be smart too to have a service discovery endpoint (running Consul, EtcD, etc.) so that a script could be run when the machine is first created to join the Docker Swarm cluster. The auto-scaling group would be configured with CloudWatch alarms to monitor CPU and RAM on the worker nodes to scale up and scale down the amount of worker nodes when needed.

**StackStorm**

Another more interesting product is [StackStorm](https://stackstorm.com/), which is an open-source product, and helps you do auto-scaling with Docker Swarm based on metrics with Docker Swarm itself. The product runs as a container, and watches the `Pending tasks` queue in Docker Swarm. If there is a request to scale up a service, or a new service is being deployed, StackStorm will detect that the current nodes do not have enough resources, and spin up a new worker EC2 node.

You can see it here: [StackStorm demonstration](https://www.youtube.com/watch?v=jxgdznuepT8)

### The role of Terraform / CloudFormation

For Option 2, having a Terraform state file, of CloudFormation YAML / JSON definition is also very important. For auto-scaling it's obviously not needed since the cluster will scale up and down as needed. However, it's still important to keep an up-to-date definition of your entire infrastructure. This has many benefits, including:

* Being able to spin up your infrastructure in new regions
* Having the ability to create a new cluster for blue/green deployments when Docker has a major update
* General backup purposes

Option 3: Self-Hosted in Datacenter
-------------

Options 1 and 2 are assuming you're running everything on a cloud provider, but what if Dalia wants to have some apps running on bare-metal? Or what if Dalia simply wants disaster recovery?

If you're using a Docker-based solution, then having an emergency / co-location using bare-metal makes this fairly easy, and there are a number of open-source tools that you can use. Investing in a product like VMWare ESXi / Citrix XenServer makes sense if you have the funds available, but the following alternatives are attractive as well:

* Mist.io: A company that provides an [open-source](https://github.com/mistio/mist.io) and [SaaS-based](https://mist.io/product) tool to monitor and manage your hybrid infrastructure.
* OpenStack or Apache CloudStack: Open-source alternatives to paid clustering tools.
* Pure bare-metal machines with Linux: Although it requires more management, the beauty of Docker Swarm is that it doesn't require much in terms of setup, and thus, can simply be ran on machines running Linux directly.

In this scenario, a datacenter-based solution would look like this:

[![Self-hosted DC](https://gitlab.com/sc250024/dalia-tech-challenge/uploads/e38b3145ba28082aa142cc53aba2b568/dalia_autoscaling_selfhosted_dc_small.png)](https://gitlab.com/sc250024/dalia-tech-challenge/uploads/92fb8dd06a0a05816cd764b8c6f7756c/dalia_autoscaling_selfhosted_dc.png)

It looks more or less the same as the cloud budget scenario. Auto-scaling in this scenario would be more difficult than using AWS or Google Cloud Engine, but it is still possible. Terraform could be used as a way to provision new machines since it has provider plug-ins for VMWare, OpenStack, and CloudStack. Spinning up new virtual Docker Swarm workers is possible with monitoring and scripting.


Q & A
-------------

### How would you configure the infrastructure so it grows automatically when more resources are needed?

I outlined some of the methods above, but in summary they are:

* Cloud Premium: abstract away the need to provision machines ourselves to the provider, such as Docker DC
* Cloud Budget: utilize AWS auto-scaling groups (or equivalent in GCE)
* Self-hosted in Datacenter: with a fixed number of physical machines, in my opinion the pressure to scale-up and then scale-down as needed becomes less important since these costs are more fixed. Scaling up your virtual servers that are running as Docker Swarm workers is much more crucial than scaling down.

### What will be the triggers to decide when the infrastructure has to add more resources?

Besides the obvious metrics of CPU and RAM, much more interesting would be to monitor Docker Swarm events (like StackStorm does) so that scaling happens in response to Docker events rather than OS performance.

For CPU and RAM, I would recommend scaling up when utilization hits around 70 percent. Whether they are virtual machine clusters or Docker Swarm clusters, it's common to run out of RAM first before you hit a limit on CPU.

Docker Swarm also [has a couple strategies](https://github.com/docker/docker.github.io/blob/master/swarm/scheduler/strategy.md) on how it distributes containers in the cluster. Whether the Swarm is using `binpack` or `spread` (default) should also be considered.

### How will the deployment process be for new releases of the app?

This very much depends on the processes that have been developed at the company already, how the app works, how it's tested, the merging / branching strategy, and many other factors. Assuming that we could start from scratch, I would implement the following type of process:

1. New feature is developed in a feature branch using the [GitHub Flow model](https://guides.github.com/introduction/flow/).
2. Feature is reviewed and testing by QA team (if exists) for acceptance testing. Feature is also reviewed by another team member, or senior team member.
3. DevOps also looks at the PR, checking for proper integration tests were written, and other things that might cause issues during deployment (a large database migration, for example).
4. Final changes are made to the code, committed, and pushed to the feature branch. The repository should be automatically linked with a CI platform; this could be self-hosted (Jenkins) or paid (CircleCI). CI performs unit, integration, and system testing. The CI platform should be capable of spinning up necessary test infrastructure for testing as well.
5. CI finishes and passes, and the CI platform builds the Docker image(s). Docker images are pushed to the registry. Hook is triggered with CD platform.
6. CD platform receives the hook to deploy the new version of the Docker image. The deployment is set up in such a way as to do rolling deployments in order to spot-check during roll-out.
7. Assuming deployment was successful, feature branch is now merged into master, becoming the latest stable version of the code.

### How do you monitor the new infrastructure if something stops working? In which situations can this possibly happen?

Monitoring is **very** important in making sure your application is working as it was intended. This is a very large question to tackle, but generally, you need 4 levels of monitoring in your company:

1. **OS level / basic application level monitoring:** For the OS, this involves checking the typical metrics of CPU load, free RAM, disk space, etc. The application should also be monitored for HTTP(S) connectivity for each endpoint, as well as TCP connectivity for the underlying back-end services that the application requires.
2. **Log monitoring:** A tool like ELK stack, or at the very least, something like [Papertrail](https://papertrailapp.com/) to aggregate logs is helpful for getting a quick view of what the problem might be.
3. **Stack trace monitoring:** At Vacasa we used [Sentry](https://sentry.io/welcome/), which was such an invaluable tool to spot errors in the application before they became a huge problem. Sentry actually became for us the first indicator that something was wrong since the code itself would report to Sentry if something was not working.
4. **Database monitoring:** A problem we faced at Vacasa many times was bad performance from long SQL queries. As your application grows larger and larger, SQL refactoring becomes very important. A tool like [MONyog](https://www.webyog.com/product/monyog) or [VividCortex](https://www.vividcortex.com/) helps greatly in this arena.

### What kind of hardware is the optimal one? (In terms of HD type, RAM, etc.). What is the most relevant for this kind of service?

If you're using AWS you don't need to worry typically about the hardware since they run it for you. With EC2 machines, the factors you can control are a.) the number of CPU cores, b.) RAM, and c.) disk IOPS. If you're running a high-performance application, disk IOPS and what numbers to use becomes very important.

This is a different question when running your own datacenter. With any cluster management software you use, you'll need one or two dedicated storage arrays. Preferably, these should be loaded with SSD disks, or at the very least 15,000 RPM SCSI drives. The arrays in this case are usually set to RAID5 or RAID6 for handling drive failures. FibreChannel vs. iSCSI is also something that can be considered.

### What type of backups do you recommend? How would you implement them?

For code, a Git server takes care of all versions of your code. The reason why I advocated for the [GitHub Flow](https://guides.github.com/introduction/flow/) model was because branches are deployed **first**, and only when the deployment is successful does the code get merged into the master branch. This ensures easy rollback to the previous version.

If you're running a Docker cluster on AWS, backups generally aren't needed that often. In fact, let me state this here unequivocally: **Servers are not snowflakes, and treating them as such will give you massive headaches for scaling.** Doing things like storing static files on web servers is a big no-no! Developers should always be storing files in S3, for example. In cloud environments, usually the items that need backups are databases, in which case, it's smart to *at least* take daily snapshots.

Running your own hardware is a different scenario; backups are **much** more important. I would recommend the following strategies:

* RAID6 for storage arrays. The slight performance loss from having the array CPU take more time to calculate redundancy is worth it because it means 2 disks can fail before there's a serious problem. With SSD disks as well, the performance hit is negligible for most applications.
* Off-site backup, using site-to-site VPNs between your main datacenter and a co-location (the office itself perhaps?). Most backup software is smart enough so that after the initial large sync of your data, moving forward only the incremental changes are synchronized.

### How would you confirm that the new setup is supporting the amount of traffic it is intended to support?

Monitoring solutions will tell you if your cluster, or your servers, are being overloaded.

There is another metric that can be considered: web page loading times. If your site is receiving too much traffic, web page load times will start to plummet severely. The infrastructure itself could be find, but there might be a problem with the application itself.