Scott Crooks - Dalia Tech Challenge
===================

----------

# Practical Challenge

For the practical challenge, we are trying to achieve this kind of result:

> We want to have a system to create the needed environment in any kind of instance, this can be in our local laptop, in a hosted server, in a AWS EC2 instance, etc. Create a process that helps us to deploy this app just running a simple command.

I found this interesting because you all chose a sample application in the Heroku repository ([https://github.com/heroku/ruby-rails-sample](https://github.com/heroku/ruby-rails-sample)), and the `heroku` CLI utility is *exactly* designed for the purpose you want to achieve: deploying an app locally, or to a Heroku cloud environment from the command line. I am making the assumption that a CLI utility like this is desired.

Tools / Utilities / Config files needed
-------------

* [Docker for Mac](https://docs.docker.com/docker-for-mac/)
* [ElasticBeanstalk (EB) CLI Utility](http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/eb-cli3.html)
* [Packages for macOS](http://s.sudre.free.fr/Software/Packages/about.html)
* AWS credentials located in `~/.aws/credentials`
* The docker file `Dockerfile` located in the repo root
* Docker Compose file `docker-compose.yml` located in the repo root

AWS credentials are stored locally in the following format:

```
[default]
aws_access_key_id = AKIAXXXXXXXXXXXXXX5A
aws_secret_access_key = Js8VREq98RCuBfH7sdUSzwf9/v53xTFk8q7G82C
```

A `Dockerfile` for RoR might look like this:

```
FROM ruby:2.X.Y
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /myapp
WORKDIR /myapp
ADD Gemfile /myapp/Gemfile
ADD Gemfile.lock /myapp/Gemfile.lock
RUN bundle install
ADD . /myapp
```

A `docker-compose.yml` file for RoR might look like this:

```
version: '3'
services:
  db:
    image: postgres
  web:
    build: .
    command: bundle exec rails s -p 3000 -b '0.0.0.0'
    volumes:
      - .:/myapp
    ports:
      - "3000:3000"
    depends_on:
      - db
```

Assumptions
-------------

* Local and remote deployments are Docker-based
* http://myapp.daliaresearch.com is a production-grade service that needs proper CI and testing.

Questions to consider
-------------

### Proper CLI utility app, or BASH scripts?

Unfortunately AWS doesn't have *as nice* of a slick CLI utility like Heroku, at least not for their general AWS CLI Utility. They *do* have a dedicated CLI utility for their ElasticBeanstalk offering (referred to as `eb`), which would serve nicely for deploying an ElasticBeanstalk-based app, and which functions very similarly to the Heroku based app.

One way to go about this would be to simply use a tool like **Packages** for macOS to distribute a CLI utility to all developers. The "utilty" could simply be a set of BASH scripts which would serve as wrappers for other CLI utilities: `docker-machine`, `eb`, `docker`, `vagrant`, etc. This is similar to what myself and another DevOps engineer did at Vacasa. The CLI utility lacked a deploy option, but that had to do more with our deployment strategy; we still had a monolithic codebase, so deployments were more complicated. Developers could still spin up local environments, and could also create QA machines based in AWS that would expire after 2 weeks. This assumed that Docker for Mac and Vagrant were already pre-installed.

Another route would be to develop a CLI utility in a language like Golang. I actually prefer this option, but it would take some time to develop. The main reason is that a utility built this way could be self-contained. AWS libraries or Vagrant libraries could be bundled into the compiled binary itself, without relying on a developer to install the majority of the surround utilities themselves.

Deployment process
-------------

Local and remote deployments would be treated very differently. Remote deployments would still need to go through the proper deployment process

Assuming we are running a Golang-based CLI utility, the app would generally look something like the following for deployments.

* Local deployments
  * Use a `local` flag just like Heroku and the EB utility. Calling `dalia local run` inside of an appropriate code repository would initiate a local deployment.
  * Perform checks to make sure `docker-compose` is installed.
  * Check for existence of `docker-compose.yml` file to ensure the appropriate resources are spun up (naming of `docker-compose` files would need to be standardized in naming for local / production environments).
  * Run `docker-compose up -d` in the background through the utility.
  * Output the appropriate local address for viewing the app (http://localhost:8080/)
* Remote deployments
  * When I read the description for this challenge, I was a bit curious why we would want to deploy an app directly to production through a CLI utility. For local testing, or a remote QA machine this is fine, but production deployments should go through the proper CI/CD pipeline. This means actually you *wouldn't* want to deploy a production-grade app through a CLI utility. Deployments should be initiated through a merge to master on GitHub / GitLab, which then triggers a series of hooks that perform CI, building, and then continuous deployment to production.
